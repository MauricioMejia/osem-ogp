#! /usr/bin/env python3


import argparse
from collections import OrderedDict
import copy
import csv
import json
import os
import re
import sys
import time

import mysql.connector


config = {
    'user': 'root',
    'password': '',
    'host': '127.0.0.1',
    'database': 'osem_prod',
    'raise_on_warnings': True,
    # 'use_pure': False,
    }

csv_names = (
    'id_link',
    'group.id',
    'track',
    'event_type_id_and_title',
    'language',
    'submitter.name',
    'submitter.email',
    'submitter.affiliation',
    'title',
    )

event_names = (
    'id',
    'guid',
    'event_type_id',
    'title',
    'subtitle',
    'time_slots',
    'state',
    'progress',
    'language',
    'start_time',
    'abstract',
    'description',
    'public',
    'logo_file_name',
    'logo_content_type',
    'logo_file_size',
    'logo_updated_at',
    'proposal_additional_speakers',
    'track_id',
    'room_id',
    'created_at',
    'updated_at',
    'require_registration',
    'difficulty_level_id',
    'week',
    'is_highlight',
    'program_id',
    'expected_audience',
    'objective',
    'contact_phone_number',
    )

event_type_names = (
    'id',
    'title',
    'length',
    'minimum_abstract_length',
    'maximum_abstract_length',
    'color',
    'description',
    'program_id',
    'minimum_objective_length',
    'maximum_objective_length',
    )

event_user_names = (
    'id',
    'user_id',
    'event_id',
    'event_role',
    'comment',
    'created_at',
    'updated_at',
    )

group_id_by_track_id = {
    None: 3,
    3: 3,  # Climate and sustainable development
    4: 1,  # Transparency, accountability and fight against corruption
    5: 3,  # Digital and development
    6: 2,  # Civic tech and participatory tools
    7: 2,  # Open data and Open resources
    8: 3,  # Public innovation
    9: 4,  # Open government for cities
    10: 4,  # Open parliament
    11: 1,  # Access to information
    12: 4,  # Fundamentals liberties and human rights
    14: 3,  # Regional focus and Francophonie
    15: 4,  # Implementation of open government
    }

group_title_by_id = {
    1: "Public and economic sector transparency",
    2: "Digital",
    3: "Climate change and sustainable development",
    4: "Implementation of open government and new actors",
    }

track_names = (
    'id',
    'guid',
    'name',
    'description',
    'color',
    'created_at',
    'updated_at',
    'program_id',
    )

user_names = (
    'id',
    'email',
    'encrypted_password',
    'reset_password_token',
    'reset_password_sent_at',
    'remember_created_at',
    'sign_in_count',
    'current_sign_in_at',
    'last_sign_in_at',
    'current_sign_in_ip',
    'last_sign_in_ip',
    'confirmation_token',
    'confirmed_at',
    'confirmation_sent_at',
    'unconfirmed_email',
    'created_at',
    'updated_at',
    'name',
    'email_public',
    'biography',
    'nickname',
    'affiliation',
    'avatar_file_name',
    'avatar_content_type',
    'avatar_file_size',
    'avatar_updated_at',
    'mobile',
    'tshirt',
    'languages',
    'volunteer_experience',
    'is_admin',
    'username',
    'is_disabled',
    )

vote_names = (
    'id',
    'event_id',
    'rating',
    'created_at',
    'updated_at',
    'user_id',
    )


parser = argparse.ArgumentParser()
parser.add_argument('-v', '--verbose', action = 'store_true', default = False, help = "increase output verbosity")
args = parser.parse_args()


connection = mysql.connector.connect(**config)
cursor = connection.cursor()


query = ("""\
    SELECT * FROM event_types
    """)
cursor.execute(query)
event_type_by_id = {
    event_type['id']: event_type
    for event_type in (
        OrderedDict(
            (name, value)
            for name, value in zip(event_type_names, row)
            if value is not None
            )
        for row in cursor
        )
    }
print('event_type_by_id = {}'.format(len(event_type_by_id)))

query = ("""\
    SELECT * FROM event_users
    """)
cursor.execute(query)
event_users_by_event_id = {}
for event_user in (
        OrderedDict(
            (name, value)
            for name, value in zip(event_user_names, row)
            if value is not None
            )
        for row in cursor
        ):
    event_users_by_event_id.setdefault(event_user['event_id'], []).append(event_user)
print('event_users_by_event_id = {}'.format(len(event_users_by_event_id)))

query = ("""\
    SELECT * FROM events where state = 'new'
    """)
cursor.execute(query)
events = [
    OrderedDict(
        (name, value)
        for name, value in zip(event_names, row)
        if value is not None
        )
    for row in cursor
    ]
print('events = {}'.format(len(events)))

query = ("""\
    SELECT * FROM tracks
    """)
cursor.execute(query)
track_by_id = {
    track['id']: track
    for track in (
        OrderedDict(
            (name, value)
            for name, value in zip(track_names, row)
            if value is not None
            )
        for row in cursor
        )
    }
print('track_by_id = {}'.format(len(track_by_id)))

query = ("""\
    SELECT * FROM users
    """)
cursor.execute(query)
user_by_id = {
    user['id']: user
    for user in (
        OrderedDict(
            (name, value)
            for name, value in zip(user_names, row)
            if value is not None
            )
        for row in cursor
        )
    }
print('user_by_id = {}'.format(len(user_by_id)))

query = ("""\
    SELECT * FROM votes
    """)
cursor.execute(query)
votes_by_event_id = {}
for vote in (
        OrderedDict(
            (name, value)
            for name, value in zip(vote_names, row)
            if value is not None
            )
        for row in cursor
        ):
    votes_by_event_id.setdefault(vote['event_id'], []).append(vote)
print('votes_by_event_id = {}'.format(len(votes_by_event_id)))

cursor.close()
connection.close()

field_names = [
    {
        'event_type_id': 'event_type',
        'id': 'url',
        'track_id': 'track',
        }.get(name, name)
    for name in event_names
    if name not in (
        'guid',
        'state',
        'progress',
        'public',
        'require_registration',
        'week',
        'is_highlight',
        'program_id',
        )
    ] + [
    'group.id',
    'group.title',
    'user.event_role',
    ] + [
    'user.{}'.format({
        'id': 'url',
        }.get(name, name))
    for name in user_names
    if name not in (
        'created_at',
        'updated_at',
        'encrypted_password',
        'reset_password_token',
        'reset_password_sent_at',
        'remember_created_at',
        'sign_in_count',
        'current_sign_in_at',
        'last_sign_in_at',
        'current_sign_in_ip',
        'last_sign_in_ip',
        'confirmation_token',
        'confirmed_at',
        'confirmation_sent_at',
        'unconfirmed_email',
        'avatar_file_name',
        'avatar_content_type',
        'avatar_file_size',
        'avatar_updated_at',
        'mobile',
        'tshirt',
        'languages',
        'volunteer_experience',
        'is_admin',
        'username',
        'is_disabled',
        )
    ] + [
    'admin_rating_count',
    'admin_rating',
    'non_admin_rating_count',
    'non_admin_rating',
    ]
entries = []
for event in events:
    event_id = event['id']
    entry = {}
    entry.update(event)
    entry['event_type'] = event_type_by_id[entry['event_type_id']]['title']
    entry['event_type_id_and_title'] = '{} - {}'.format(entry['event_type_id'], entry['event_type'])
    track_id = entry.get('track_id')
    entry['group.id'] = group_id = group_id_by_track_id[track_id]
    entry['group.title'] = group_title_by_id[group_id]
    if track_id is not None:
        entry['track'] = track_by_id[track_id]['name']
    entry['short_url'] = 'https://ogpsummit.org/osem/conference/ogp-summit/program/proposal/{}'.format(event_id)
    entry['title'] = ' '.join(entry['title'].split()).strip().rstrip('.')
    entry['url'] = event_url = 'https://en.ogpsummit.org/osem/conference/ogp-summit/program/proposal/{}'.format(
        event_id)
    entry['id_link'] = '=HYPERLINK("{}", "{}")'.format(event_url, event_id)

    event_roles = []
    users = []
    for event_user in event_users_by_event_id[event_id]:
        user = user_by_id[event_user['user_id']]
        if user not in users:
            event_roles.append(event_user['event_role'])
            users.append(user)
    submitter = None
    users = copy.deepcopy(users)
    for user, event_role in zip(users, event_roles):
        user['event_role'] = event_role
        if submitter is None and event_role == 'submitter':
            submitter = user
        user['url'] = 'https://fr.ogpsummit.org/osem/users/{}'.format(user.pop('id'))
    assert submitter is not None
    entry['submitter'] = submitter
    entry['users'] = users
    merged_user_names = [
        {
            'id': 'url',
            }.get(name, name)
        for name in user_names
        ]
    for name in merged_user_names:
        if submitter.get(name) is not None:
            entry['submitter.{}'.format(name)] = submitter[name]
    merged_user = OrderedDict(
        ('user.{}'.format(merged_name), merged_value)
        for merged_name, merged_value in (
            (
                name,
                '\n'.join(
                    str(value) if value is not None else ''
                    for value in (
                        user.get(name)
                        for user in users
                        )
                    ).rstrip(),
                )
            for name in merged_user_names
            )
        if merged_value
        )
    merged_user['user.event_role'] = '\n'.join(event_roles)
    entry.update(
        (name, value)
        for name, value in merged_user.items()
        if name in field_names
        )

    admin_ratings = []
    non_admin_ratings = []
    for vote in votes_by_event_id.get(event_id, []):
        user = user_by_id[vote['user_id']]
        if user['is_admin']:
            admin_ratings.append(vote['rating'])
        else:
            non_admin_ratings.append(vote['rating'])
    entry['admin_rating'] = sum(admin_ratings) / len(admin_ratings) if admin_ratings else 0
    entry['admin_rating_count'] = len(admin_ratings)
    entry['non_admin_rating'] = sum(non_admin_ratings) / len(non_admin_ratings) if non_admin_ratings else 0
    entry['non_admin_rating_count'] = len(non_admin_ratings)

    entries.append(entry)

for index, name in reversed(list(enumerate(field_names[:]))):
    for entry in entries:
        if entry.get(name) not in (None, ''):
            break
    else:
        del field_names[index]

entries.sort(key = lambda entry: (
    entry['group.id'],
    entry['event_type_id'],
    -entry['admin_rating'],
    -entry['non_admin_rating'],
    entry['id'],
    ))


# Generate CSV files.


with open('proposals-with-emails.csv', 'w') as csv_file:
    writer = csv.writer(csv_file)
    writer.writerow(csv_names)
    for entry in entries:
        writer.writerow([
            entry[name] if entry.get(name) is not None else ''
            for name in csv_names
            ])
