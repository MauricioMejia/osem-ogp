class AddParticipationQuestion < ActiveRecord::Migration
  def change
    qtype_single = QuestionType.find_or_create_by!(title: 'Single Choice')
    
    answer_t1 = Answer.find_or_create_by!(title: 'Attendee')
    answer_t2 = Answer.find_or_create_by!(title: 'Speaker')
    answer_t3 = Answer.find_or_create_by!(title: 'Session organizer')
    
    q = Question.find_or_initialize_by(title: 'Are you intending to participate as:', question_type_id: qtype_single.id, global: true)
    q.answers = [answer_t1, answer_t2, answer_t3]
    q.save!
  end
end
