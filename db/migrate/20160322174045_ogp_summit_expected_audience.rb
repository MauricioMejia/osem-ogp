class OgpSummitExpectedAudience < ActiveRecord::Migration
  def change
    add_column :events, :expected_audience, :integer
  end
end
