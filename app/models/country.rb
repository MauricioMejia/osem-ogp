class Country < ActiveRecord::Base
  
  def get_label(lang)
    if lang == 'fr'
      return label_fr
    else 
      return label_en
    end
  end
end
