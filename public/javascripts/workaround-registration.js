var array = [
    {
        id: "23",
        en: "venue to be confirmed",
        fr: "salle à confirmer",
        capacity: 400
    },
    {
        id: "24",
        en: "Salle Pleyel",
        fr: "Salle Pleyel",
        capacity: 2000
    },
    {
        id: "25",
        en: "Palais de Iéna and Palais de Tokyo",
        fr: "Palais de Iéna et Palais de Tokyo",
        capacity: 1400
    },
    {
        id: "47",
        en: "Assemblée Nationale",
        fr: "Assemblée Nationale",
        capacity: 450
    },
    {
        id: "49",
        en: "Sénat",
        fr: "Sénat",
        capacity: 200
    },
    {
        id: "26",
        en: "Palais de Iéna",
        fr: "Palais de Iéna",
        capacity: 500
    },
    {
        id: "48",
        en: "Palais de Iéna and Palais de Tokyo",
        fr: "Palais de Iéna et Palais de Tokyo",
        capacity: 1400
    },
    {
        id: "27",
        en: "Palais de Iéna",
        fr: "Palais de Iéna",
        capacity: 400
    },
    {
        id: "46",
        en: "Hôtel de Ville",
        fr: "Hôtel de Ville",
        capacity: 1200
    },
    {
        id: "28",
        en: "Hôtel de Ville",
        fr: "Hôtel de Ville",
        capacity: 1200
    }
];

var capacity = {
    en: "Capacity of the venue:",
    fr: "Capacité de la salle:"
};

var attendees = {
    en: "Interested participants:",
    fr: "Nombre de personnes intéressées :"
};

$(function () {
    var _move = function (beforeId, moveId) {
      var $beforeQ =  $("#registration_qanswer_ids_" + beforeId).parent().parent();
      var $moveQ = $("#registration_qanswer_ids_" + moveId).parent().parent();
      $moveQ.detach().insertAfter($beforeQ);
    };
    _move("25", "47");
    _move("26", "48");
    _move("46", "28");
    _move("47", "49");
    for(var i = 0, len=array.length; i < len; i++) {
        var obj = array[i];
        var attendeeCount = COUNT[obj.id];
        if (!attendeeCount) {
            attendeeCount = 0;
        }
        var $label =  $("#registration_qanswer_ids_" + obj.id).parent();
        var html = "<span>, <small>" + "<span><em>" + obj[LANG] + "</em></span><br>(";
        html += "<span";
        if (attendeeCount > obj.capacity) {
            html += " style='color: red'";
        }
        html += ">";
        html += "<span>" + capacity[LANG] + "</span> ";
        html += obj.capacity;
        html += " / ";
        html += "<span>" + attendees[LANG] + "</span> ";
        html += attendeeCount;
        html += "</span>";
        html += ")</small></span>";
        $label.after(html);
    }
});





