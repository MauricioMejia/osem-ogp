<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:svg="http://www.w3.org/2000/svg" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" exclude-result-prefixes="svg #default">

<xsl:output method="xml" encoding="UTF-8" indent="yes" />

<xsl:template match="/">
    <xsl:apply-templates select="*"/>
</xsl:template>

<xsl:template match="svg:tspan">
    <xsl:copy>
        <xsl:for-each select="@*">
            <xsl:if test="name() != 'x'">
                <xsl:attribute name="{name()}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:if>
        </xsl:for-each>
        <xsl:apply-templates select="*|text()"/>
    </xsl:copy>
</xsl:template>

<xsl:template match="@*|node()">
    <xsl:copy>
        <xsl:apply-templates select="*|@*|text()"/>
    </xsl:copy>
</xsl:template>



</xsl:stylesheet>
